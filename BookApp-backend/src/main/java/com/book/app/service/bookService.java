package com.book.app.service;

import java.util.List;
import org.springframework.http.ResponseEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.book.app.Book;
import com.book.app.exception.ResourceNotFoundException;
import com.book.app.repo.bookRepository;

@Service
public class bookService {
	
	@Autowired
	private bookRepository repo;
	
	public List<Book> getAllBooks(){
		return repo.findAll();
	}
	
	public Book getOneBook(Long id) {
		return repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
	}
	
	public Book addBook(Book book) {
		return repo.save(book);
	}
	
	public Book updateBook(Book book, Long id) {
		Book _book = repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
		_book.setIsbn(book.getIsbn());
		_book.setTitle(book.getTitle());
		_book.setAuthor(book.getAuthor());
		_book.setDescription(book.getDescription());
		
		return repo.save(_book);
	}
	
	public ResponseEntity<?> deleteBook(Long id) {
		Book _book = repo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
		repo.delete(_book);
		return ResponseEntity.ok().build();
	}

	
}
