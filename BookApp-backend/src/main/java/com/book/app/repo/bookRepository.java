package com.book.app.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.book.app.Book;

public interface bookRepository extends JpaRepository<Book, Long> {

}
