package com.book.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.book.app.Book;
import com.book.app.service.bookService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class bookController {
	
	@Autowired
	private bookService service;
	
	@RequestMapping("/")
	public String hello() {
		return "hello";
	}
	
	@GetMapping("/books")
	public List<Book> getAllBooks(){
		return service.getAllBooks();
	}
	
	@GetMapping("/books/{id}")
	public Book getById(@PathVariable Long id) {
		return service.getOneBook(id);
	}
	
	@PostMapping("/books")
	public Book postBook(@Valid @RequestBody Book book){		
		return service.addBook(book);
	}
	
	@PutMapping("/books/{id}")
	public Book updateBook(@Valid @RequestBody Book book, @PathVariable Long id) {
		return service.updateBook(book, id);
	}
	
	@DeleteMapping("/books/{id}")
	public void deleteBook(@PathVariable Long id) {
		service.deleteBook(id);
	}
	
}
