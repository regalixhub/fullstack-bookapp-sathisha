import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Book } from './../model/book';


@Injectable({
  providedIn: 'root'
})

export class BookService {

  private baseUrl = 'http://localhost:8080/books';
  private headers = new HttpHeaders({ 'Content-type': 'application/json' });
  private options = { headers: this.headers };

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(this.baseUrl + '/', this.options);
  }

  getBookById(id: number): Observable<Book> {
    return this.http.get<Book>(this.baseUrl + '/' + id, this.options);
  }

  addBook(book: Book): Observable<Book> {
     return this.http.post<Book>(this.baseUrl + '/', JSON.stringify(book), this.options);
  }

  updateBook(book: Book): Observable<Book> {
    return this.http.put<Book>(this.baseUrl + '/' + book.id, JSON.stringify(book), this.options);
  }

  deleteBook(id: number) {
    return this.http.delete(this.baseUrl + '/' + id, this.options);
  }

}
