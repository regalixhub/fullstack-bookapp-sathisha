import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { BookService } from './../../service/book.service';
import { Book } from './../../model/book';

@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.css']
})
export class BookListItemComponent {

  changeText = false;

  @Input() public books: Book[] = [];
  constructor(private router: Router,
              private bookService: BookService) { }

  detail(id: number) {
    this.router.navigate(['/details', id]);
  }

  edit(id: number) {
    this.router.navigate(['/edit', id]);
  }

  delete(id: number, i: number) {
    this.bookService.deleteBook(id).subscribe(() => {
      this.books.splice(i, 1);
    }, (error) => {
      this.router.navigate(['/error']);
    });
  }

}
