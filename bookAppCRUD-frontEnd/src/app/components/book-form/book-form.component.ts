import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Book } from './../../model/book';
import { BookService } from './../../service/book.service';


@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent {

  constructor(private bookService: BookService,
    private router: Router) { }

  public book = new Book(0, '', '', '', '');
  public nija = true;

  onClickSubmit() {
      this.bookService.addBook(this.book).subscribe(() => {
        this.router.navigate(['/list']);
      }, (error) => {
        this.router.navigate(['/error']);
      });
  }

}
