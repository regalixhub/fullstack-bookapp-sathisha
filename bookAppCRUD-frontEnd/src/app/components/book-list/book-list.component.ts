import { Component, OnInit } from '@angular/core';

import { Book } from './../../model/book';
import { BookService } from './../../service/book.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-book-list',
  template: `<app-book-list-item [books]="books"></app-book-list-item>`,
})
export class BookListComponent implements OnInit {

  public books: Book[] = [];

  constructor(private bookService: BookService, private router: Router) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe((data) => {
      this.books = data;
      this.books.sort(function (a, b) {
        return a.id - b.id;
      });
    }, (error) => {
      console.error(error);
      this.router.navigate(['/error']);
    });
  }
}
