1. clone project.
2. BookApp back-end is for backend connectivity
3. bookAppCRUD-frontend is for ui
4. cd into the backend code folder run it using ./gradlew bootRun
5. cd into the front-end folder and install (  npm install --save-dev @angular-devkit/build-angular  )
6. run front-end app using ng serve
7. browse http://localhost4200 in browser and access the ui
